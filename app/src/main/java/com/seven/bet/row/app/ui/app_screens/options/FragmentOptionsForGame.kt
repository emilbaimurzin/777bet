package com.seven.bet.row.app.ui.app_screens.options

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.seven.bet.row.app.databinding.FramentOptionsForGameBinding
import com.seven.bet.row.app.domain.Sasdfklafklsdf
import com.seven.bet.row.app.ui.other.MainContainerActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FragmentOptionsForGame : Fragment() {
    private lateinit var dfssdhkfjds: FramentOptionsForGameBinding

    private val dhbfhjsdf by lazy {
        if (1 == 1) "dasasdasdasdd" else "dsdadsdas"
        Sasdfklafklsdf(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (1 == 1) "dasasdasdasdd" else "dsdadsdas"
        dfssdhkfjds = FramentOptionsForGameBinding.inflate(layoutInflater)
        if (1 == 1) "dasasdasdasdd" else "dsdadsdas"
        if (1 == 1) "dasasdasdasdd" else "dsdadsdas"
        return dfssdhkfjds.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnTouchListeners()
        dfssdhkfjds.backddsfdsdsfdsf.setOnClickListener {
            if (1 == 1) "dasasdasdasdd" else "dsdadsdas"
            findNavController().popBackStack()
            if (1 == 1) "dasasdasdasdd" else "dsdadsdas"
        }


        lifecycleScope.launch {
            delay(20)
            val percentage: Double = dfssdhkfjds.musicSliderBgDsdfdsfsdfsfd.width.toDouble() / 100.0
            val lp1 = dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams
            lp1.width = when {
                dhbfhjsdf.dfshjsjdkhjkdsf() < 0 -> 1
                dhbfhjsdf.dfshjsjdkhjkdsf() > 100 -> dfssdhkfjds.musicSliderBgDsdfdsfsdfsfd.width
                else -> (percentage * dhbfhjsdf.dfshjsjdkhjkdsf()).toInt()
            }
            dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams = lp1

            val lp2 = dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams
            lp2.width = when {
                dhbfhjsdf.kldjfklsdjfdsjklfs() < 0 -> 1
                dhbfhjsdf.kldjfklsdjfdsjklfs() > 100 -> dfssdhkfjds.musicSliderBgDsdfdsfsdfsfd.width
                else -> (percentage * dhbfhjsdf.kldjfklsdjfdsjklfs()).toInt()
            }
            dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams = lp2
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setOnTouchListeners() {
        dfssdhkfjds.musicSliderBgDsdfdsfsdfsfd.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    val percentage: Double =
                        dfssdhkfjds.musicSliderBgDsdfdsfsdfsfd.width.toDouble() / 100.0
                    val volume: Double =
                        dfssdhkfjds.musicSliderDsdfdsfsdfsfd.width.toDouble() / percentage
                    dhbfhjsdf.dsjkfsklfjdsf(volume.toInt())

                    (requireActivity() as MainContainerActivity).dfsghsdfhjdshjfdshj()
                }

                MotionEvent.ACTION_DOWN -> {
                    if (v.x > event.x + convertDpToPixel(50)) {
                        val lp = dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = 1
                        dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else if (v.x + v.width < event.x) {
                        val lp = dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (v.x + v.width).toInt()
                        dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else {
                        val lp = dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (event.x - v.x + convertDpToPixel(50)).toInt()
                        dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams = lp
                    }
                }

                MotionEvent.ACTION_MOVE -> {
                    if (v.x > event.x + convertDpToPixel(50)) {
                        val lp = dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = 1
                        dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else if (v.x + v.width < event.x) {
                        val lp = dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (v.x + v.width).toInt()
                        dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else {
                        val lp = dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (event.x - v.x + convertDpToPixel(50)).toInt()
                        dfssdhkfjds.musicSliderDsdfdsfsdfsfd.layoutParams = lp
                    }
                }

                else -> {

                }
            }
            true
        }

        dfssdhkfjds.soundSliderBgDsdfdsfsdfsfd.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_UP -> {
                    val percentage: Double =
                        dfssdhkfjds.soundSliderBgDsdfdsfsdfsfd.width.toDouble() / 100.0
                    val volume: Double =
                        dfssdhkfjds.soundSliderDsdfdsfsdfsfd.width.toDouble() / percentage
                    dhbfhjsdf.dfjhjsdkfhjkdsfkdsf(if (volume > 100) 100 else volume.toInt())
                }

                MotionEvent.ACTION_DOWN -> {
                    if (v.x > event.x + convertDpToPixel(50)) {
                        val lp = dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = 1
                        dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else if (v.x + v.width < event.x) {
                        val lp = dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (v.x + v.width).toInt()
                        dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else {
                        val lp = dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (event.x - v.x + convertDpToPixel(50)).toInt()
                        dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams = lp
                    }
                }

                MotionEvent.ACTION_MOVE -> {
                    if (v.x > event.x + convertDpToPixel(50)) {
                        val lp = dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = 1
                        dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else if (v.x + v.width < event.x) {
                        val lp = dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (v.x + v.width).toInt()
                        dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams = lp
                    } else {
                        val lp = dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams
                        lp.width = (event.x - v.x + convertDpToPixel(50)).toInt()
                        dfssdhkfjds.soundSliderDsdfdsfsdfsfd.layoutParams = lp
                    }
                }

                else -> {

                }
            }
            true
        }
    }

    private fun convertDpToPixel(dp: Int): Int {
        val resources = requireContext().resources
        val metrics = resources.displayMetrics
        return (dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }

    override fun onResume() {
        super.onResume()
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().popBackStack()
            }
        })
    }
}